import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class PopovekaTest {

    @Test
    public void factorialTest() {
        Popoveka popoveka = new Popoveka();
        int n = 3;
        long expectedValue = 6;


        long result = popoveka.factorial(n);

        Assertions.assertEquals(expectedValue, result);
        }
    }
