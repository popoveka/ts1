import PageObjectModel.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class SpringerTest {
    WebDriver driver;


    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\kasia\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void searchArticles() {
        String email = "novakpepa@mail.ru";
        String password = "pepapepa12";
        driver.get("https://link.springer.com/signup-login");

        SpringerLogin springerLogin = new SpringerLogin(driver);
        springerLogin.enterEmail(email);
        springerLogin.enterPassword(password);
        SpringerMainPage mainPage = springerLogin.loginSubmit();


        SpringerAdvancedSearch search = mainPage.navAdvancedSearch();
        search.enterAllWords("Page Object Model");
        search.enterLeastWords("Selenium Testing");
        search.setYear(2022);
        SpringerSearch articleSearch = search.submitSearch();

        articleSearch.selectArticle();
        List<String> links = articleSearch.getLinks();

        String [] expectedNames = new String[] {"APOGEN: automatic page object generator for web testing", "Model-based testing leveraged for automated web tests", "ESICM LIVES 2017", "ESICM LIVES 2020"};

        for (int i=0; i < 4; i++) {
            driver.get(links.get(i));
            SpringerArticle article = new SpringerArticle(driver);

            Assertions.assertEquals(expectedNames[i], article.getArticleName());
        }

        String [] expectedDOI = new String[] {"10.1007/s11219-016-9331-9", "10.1007/s11219-021-09575-w", "10.1186/s40635-017-0151-4", "10.1186/s40635-020-00354-8"};

        for (int i=0; i < 4; i++) {
            driver.get(links.get(i));
            SpringerArticle article = new SpringerArticle(driver);

            Assertions.assertEquals(expectedDOI[i], article.getDOI());
        }

        String [] expectedDate = new String[] {"2016-08-09", "2021-11-27", "2017-09-20", "2020-12-14"};
        for (int i=0; i < 4; i++) {
            driver.get(links.get(i));
            SpringerArticle article = new SpringerArticle(driver);

            Assertions.assertEquals(expectedDate[i], article.getDate());
        }



    }
//
//    @Test
//    public void articleOM() {
//        driver.get("https://link.springer.com/article/10.1007/s11219-016-9331-9");
//
//        SpringerArticle article = new SpringerArticle(driver);
//
//        Assertions.assertEquals("APOGEN: automatic page object generator for web testing", article.getArticleName());
//        Assertions.assertEquals("10.1007/s11219-016-9331-9", article.getDOI());
//        Assertions.assertEquals("2016-08-09", article.getDate());
//    }
//
//    @Test
//    public void springerSearchTest() {
//        driver.get("https://link.springer.com/search?query=Page+AND+Object+AND+Model+AND+%28Selenium+OR+Testing%29&date-facet-mode=in&facet-start-year=2022&showAll=true");
//        SpringerSearch search = new SpringerSearch(driver);
//       search.selectArticle();
//        List<String> links = search.getLinks();
//
//        Assertions.assertEquals("https://link.springer.com/article/10.1007/s11219-016-9331-9", links.get(0));
//
//    }






}
