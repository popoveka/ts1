package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class SpringerSearch {
    private WebDriver driver;
    private By searchTitleBy = By.className("title");

    @FindBy (xpath="//span[text() = 'Article']/..")
    private WebElement chooseArticle;

    public SpringerSearch(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void selectArticle() {
        chooseArticle.click();
    }

    public List<String> getLinks() {
        List<WebElement> linkElems = driver.findElements(searchTitleBy);
        List<String> links = new ArrayList();
        for (int i = 0; i < linkElems.size(); i++) {
            String link = linkElems.get(i).getAttribute("href");
            links.add(link);
        }
        return links;
    }








}
