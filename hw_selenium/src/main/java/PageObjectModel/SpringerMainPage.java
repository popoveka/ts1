package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpringerMainPage {

    private WebDriver driver;
    private By advancedSearchBy = By.id("advanced-search-link");

    @FindBy(css = ".pillow-btn.open-search-options")
    private WebElement settingsButton;


    public SpringerMainPage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
    }

    public SpringerAdvancedSearch navAdvancedSearch() {
       settingsButton.click();
       driver.findElement(advancedSearchBy).click();
       return new SpringerAdvancedSearch(driver);
    }


}

