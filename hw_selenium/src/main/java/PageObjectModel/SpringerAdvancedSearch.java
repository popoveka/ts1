package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SpringerAdvancedSearch {

    private WebDriver driver;


    @FindBy(id="all-words")
    private WebElement allWords;

    @FindBy(id="least-words")
    private WebElement leastWords;

    @FindBy(id="facet-start-year")
    private WebElement yearElem;

    @FindBy(id="date-facet-mode")
    private WebElement date;

    @FindBy(id="submit-advanced-search")
    private WebElement searchSubmit;



    public SpringerAdvancedSearch(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
        driver.get("https://link.springer.com/advanced-search");
    }

    public void enterAllWords(String allWordsStr) {
        allWords.sendKeys(allWordsStr);
    }

    public void enterLeastWords(String leastWordsStr) {
        leastWords.sendKeys(leastWordsStr);
    }

    public void setYear(int year) {
        Select dateChoice = new Select(date);
        dateChoice.selectByValue("in");
        yearElem.sendKeys(String.valueOf(year));
    }

    public SpringerSearch submitSearch(){
        searchSubmit.click();
        return new SpringerSearch(driver);
    }

}
