package PageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SpringerLogin {

    private WebDriver driver;

    @FindBy(id = "login-box-email")
    private WebElement email;

    @FindBy(id = "login-box-pw")
    private WebElement password;

    @FindBy(css = ".form-submit>button")
    private WebElement loginSubmit;

    @FindBy(css = ".cc-button")
    private WebElement acceptCookies;

    public SpringerLogin(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
        if (acceptCookies != null){
            acceptCookies.click();
        }
        driver.get("https://link.springer.com/signup-login");
    }

    public void enterEmail(String emailStr) {
        email.sendKeys(emailStr);
    }

    public void enterPassword(String passwordStr) {
        password.sendKeys(passwordStr);
    }

    public SpringerMainPage loginSubmit() {
        loginSubmit.click();
        return new SpringerMainPage (driver);
    }


}
