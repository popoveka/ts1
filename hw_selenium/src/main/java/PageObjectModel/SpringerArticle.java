package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.sql.Driver;

public class SpringerArticle {
    private WebDriver driver;

    @FindBy(css = "h1.c-article-title")
    private WebElement articleName;

    @FindBy(css = "li.c-bibliographic-information__list-item--doi>*>span.c-bibliographic-information__value")
    private WebElement doi;


    @FindBy(css = "li.c-article-identifiers__item>*>time")
    private WebElement dateElem;


    public String getArticleName(){
        return articleName.getText();
    }

    public String getDOI() {
        String doiUrl = doi.getText();
        return doiUrl.replaceFirst("https://doi.org/", "");
    }

    public String getDate() {
        String date = dateElem.getAttribute("datetime");
        return date;

    }

    public SpringerArticle(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }


}


