package lab03;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class FooTests {
 Foo foo;
 @BeforeEach
 public void SingleTestSetUp(){
  foo = new Foo();

 }

 //NazevMetody_TestovanyStav_Ocekavany Vystup
 @Test
 public void ReturnNumber_ReturningSpecificNumber_Five(){
 // ARRANGE
  int expectedValue = 5;

  // ACT
  int result = foo.returnNumber();

  //ASSERT
  Assertions.assertEquals(expectedValue, result);
}
 @Test
 public void GetNum_GetDefaultNumber_Zero() {
  // ARRANGE

  int expectedValue = 0;

  // ACT
  int result = foo.getNum();

  //ASSERT
  Assertions.assertEquals(expectedValue, result);
 }

 @Test
 public void Increment_IncrementedValueByOne_One(){
  int expectedValue = foo.getNum() + 1;
  foo.increment();
  Assertions.assertEquals(expectedValue, foo.getNum());

 }
@Test
 public void ExceptionsThrown_CallingMethod_Exception() throws Exception{
  int expectedValue = 0;
  Assertions.assertThrows(Exception.class, () -> {
   foo.exceptionThrown();
  });
}


}
