package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class OrderTest {
    Order orderWithState;
    Order orderWithoutState;
    Order orderNullValues;
    ArrayList<Item> itemList = new ArrayList();
    ArrayList<Item> item0List = new ArrayList();

    public OrderTest() {

    }

    @BeforeEach
    public void setup() {
        Item item = new Item(1, "shirt", 150.0F, "clothes");
        Item item0 = new Item(0, "", 0, "");
        this.itemList.add(item);
        this.item0List.add(item0);
        this.orderWithState = new Order(new ShoppingCart(this.itemList), "Ivanova", "Dvorakova Praha 2", 3);
        this.orderWithoutState = new Order(new ShoppingCart(this.itemList), "Ivanova", "Dvorakova Praha 2");
        this.orderNullValues = new Order(new ShoppingCart(this.item0List), "", "");
        }

    @Test
    public void constructor_OrderCreationStateNotZero_OrderWithGivenValues (){
        ArrayList<Item> expectedItemList = this.itemList;
        String expectedName = "Ivanova";
        String expectedAddress = "Dvorakova Praha 2";
        int expectedState = 3;

        Assertions.assertEquals(expectedItemList, this.orderWithState.getItems());
        Assertions.assertEquals(expectedName, this.orderWithState.getCustomerName());
        Assertions.assertEquals(expectedAddress, this.orderWithState.getCustomerAddress());
        Assertions.assertEquals(expectedState, this.orderWithState.getState());
    }

    @Test
    public void constructor_OrderCreationStateZero_OrderWithGivenValues (){
        ArrayList<Item> expectedItemList = this.itemList;
        String expectedName = "Ivanova";
        String expectedAddress = "Dvorakova Praha 2";
        int expectedState = 0;

        Assertions.assertEquals(expectedItemList, this.orderWithoutState.getItems());
        Assertions.assertEquals(expectedName, this.orderWithoutState.getCustomerName());
        Assertions.assertEquals(expectedAddress, this.orderWithoutState.getCustomerAddress());
        Assertions.assertEquals(expectedState, this.orderWithoutState.getState());
    }

    @Test
    public void constructor_OrderCreationNullValues_OrderWithGivenValues (){
        ArrayList<Item> expectedItemList = this.item0List;
        String expectedName = "";
        String expectedAddress = "";
        int expectedState = 0;

        Assertions.assertEquals(expectedItemList, this.orderNullValues.getItems());
        Assertions.assertEquals(expectedName, this.orderNullValues.getCustomerName());
        Assertions.assertEquals(expectedAddress, this.orderNullValues.getCustomerAddress());
        Assertions.assertEquals(expectedState, this.orderNullValues.getState());
    }
}


