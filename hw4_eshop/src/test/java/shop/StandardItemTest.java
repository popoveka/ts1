package shop;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StandardItemTest {
    StandardItem standardItem;
    
    public StandardItemTest() {
    }
    @BeforeEach
    public void setup() {this.standardItem = new StandardItem(1, "shirt", 150.0F, "clothes", 15);}

    @Test
    public void constructor_StandardItemCreation_ItemWithGivenValues() {
        int expectedID = 1;
        String expectedName = "shirt";
        float expectedPrice = 150.0F;
        String expectedCategory = "clothes";
        int expectedLoyaltyPoints = 15;

        Assertions.assertEquals(expectedID, this.standardItem.getID());
        Assertions.assertEquals(expectedName, this.standardItem.getName());
        Assertions.assertEquals(expectedPrice, this.standardItem.getPrice());
        Assertions.assertEquals(expectedCategory, this.standardItem.getCategory());
        Assertions.assertEquals(expectedLoyaltyPoints, this.standardItem.getLoyaltyPoints());

    }

    @Test
    public void copy_itemToCopy_copyOfItem() {
        StandardItem copyOfItem = this.standardItem.copy();

        Assertions.assertEquals(copyOfItem.getID(), this.standardItem.getID());
        Assertions.assertEquals(copyOfItem.getName(), this.standardItem.getName());
        Assertions.assertEquals(copyOfItem.getPrice(), this.standardItem.getPrice());
        Assertions.assertEquals(copyOfItem.getCategory(), this.standardItem.getCategory());
        Assertions.assertEquals(copyOfItem.getLoyaltyPoints(), this.standardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource ({"1, shirt, 150, clothes, 15, 1, shirt, 150, clothes, 15"})

    public void equals_identicalObjects_true(int id1, String name1, float price1, String category1, int loyaltyPoints1, int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        boolean result = item1.equals(item2);
        Assertions.assertTrue(result);
    }

    @ParameterizedTest
    @CsvSource ({"1, shirt, 150, clothes, 15, 1, shirt, 250, clothes, 15"})

    public void equals_objectsDifferentPrice_false(int id1, String name1, float price1, String category1, int loyaltyPoints1, int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        boolean result = item1.equals(item2);
        Assertions.assertFalse(result);
    }


    @ParameterizedTest
    @CsvSource ({"1, sweater, 150, clothes, 15, 1, shirt, 150, clothes, 15"})
    public void equals_objectsDifferentName_false(int id1, String name1, float price1, String category1, int loyaltyPoints1, int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        boolean result = item1.equals(item2);
        Assertions.assertFalse(result);
    }

    @ParameterizedTest
    @CsvSource ({"1, shirt, 150, clothes, 15, 1, shirt, 150, food, 15"})
    public void equals_objectsDifferentCategory_false(int id1, String name1, float price1, String category1, int loyaltyPoints1, int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        boolean result = item1.equals(item2);
        Assertions.assertFalse(result);
    }

    @ParameterizedTest
    @CsvSource ({"1, shirt, 150, clothes, 15, 3, shirt, 150, clothes, 15"})
    public void equals_objectsDifferentId_false(int id1, String name1, float price1, String category1, int loyaltyPoints1, int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        boolean result = item1.equals(item2);
        Assertions.assertFalse(result);
    }

}
