package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;

public class ItemStockTest {
    StandardItem item;
    ItemStock itemStock;


    @BeforeEach
    public void setup() {
        item = new StandardItem(1, "shirt", 150.0F, "clothes", 15);
        itemStock = new ItemStock(item);

    }

    @Test
    public void Constructor_CreatingItemStock_ItemStockCreated(){
       int expectedCount = 0;

       Assertions.assertEquals(expectedCount, itemStock.getCount());
       Assertions.assertEquals(item, itemStock.getItem());
    }

    @ParameterizedTest
    @CsvSource({"0, 0, 0", "0, -2, -2", "0, 2, 2", "0, 16, 16"})
    public void IncreaseItemCount_IncreasingGivenValues_IncreaseExpectedValue (int initCount, int increaseNumber, int result){

        itemStock.IncreaseItemCount(initCount+increaseNumber);
        Assertions.assertEquals(result, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource ({"2, 2, 0", "2, 1, 1", "100, 20, 80"})
    public void DecreaseItemCount_DecreasingGivenValues_DecreaseExpectedValue (int initCount, int decreaseNumber, int result){
        int expectedCount = result;
        itemStock.IncreaseItemCount(initCount);
        itemStock.decreaseItemCount(decreaseNumber);

        Assertions.assertEquals(expectedCount, itemStock.getCount());


    }







}
