package hw2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    Calculator calc;

    @BeforeEach
    public void SingleTestSetUp() {
        calc = new Calculator();
    }

    //NazevMetody_TestovanyStav_Ocekavany Vystup
    @Test
    public void Add_Adding15And32_47() {
        int expectedValue = 47;
        int result = calc.add(15, 32);
        assertEquals(expectedValue, result);
    }

    @Test
    public void Subtract_Subtracting13And5_8() {
        int expectedValue = 8;
        int result = calc.subtract(13, 5);
        assertEquals(expectedValue, result);
    }

    @Test
    public void Multiply_Multiply3And6_18() {
        int expectedValue = 18;
        int result = calc.multiply(3, 6);
        assertEquals(expectedValue, result);
    }

    @Test
    public void Divide_Division10And5_2() {
        int expectedValue = 2;
        int result = calc.divide(10, 5);
        assertEquals(expectedValue, result);
    }

    @Test
    public void Divide_DivisionZero_Exception() {
        Assertions.assertThrows(Exception.class, () ->
                calc.divide(1, 0));
    }
}

//    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
//    @CsvFileSource(resources = "data.csv", numLinesToSkip = 1)
//    public void add_addsAandB_returnsC(int a, int b, int c) {
//// arrange
//        Calculator calc = new Calculator();
//        int expectedResult = c;
//// act
//        int result = calc.add(a, b);
//// assert
//        assertEquals(expectedResult, result);
//    }
//
//}