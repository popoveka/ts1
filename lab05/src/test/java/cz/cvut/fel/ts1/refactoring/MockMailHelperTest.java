package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class MockMailHelperTest {
    @Test
    public void sendMail_validEmail_saveMailToDB(){
        DBManager mockedDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDbManager);
        int mailId = 1;
        Mail mailToReturn = mock(Mail.class);
        when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);

        mailHelper.sendMail(mailId);

        verify(mockedDbManager, times(1)).findMail(mailId);

    }

}
